﻿using System;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using CripInterfaceTest;
using Newtonsoft.Json;

namespace CripBroker
{
    public class CripBroker
    {
        private const string CripAddress = "http://10.42.0.1:60773/";
        private WebServiceHost _host;
        private ServiceEndpoint _sep;

        public bool CheckConnection()
        {
            string message = GetMessage(CripAddress + "alive");
            if (message == "")
            {
                return false;
            }
            return true;
        }

        private string GetMessage(string endPoint)
        {
            try
            {
                HttpWebRequest request = CreateWebRequest(endPoint);

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }

                    // grab the response  
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                        }
                    }

                    return responseValue;
                }
            }
            catch (Exception e)
            {

                return "";
            }

        }

        private HttpWebRequest CreateWebRequest(string endPoint)
        {
            var request = (HttpWebRequest)WebRequest.Create(endPoint);

            request.Method = "GET";
            request.ContentLength = 0;
            request.ContentType = "text/xml";

            return request;
        }

        public string GetTemplates()
        {
            //Console.WriteLine("Getting templates...");
            try
            {
                HttpWebRequest request = CreateWebRequest(CripAddress + "biometric/templates");

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    var responseValue = string.Empty;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }

                    // grab the response  
                    using (var responseStream = response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(responseStream))
                        {
                            responseValue = reader.ReadToEnd();
                            return responseValue;
                        }
                    }
                }
            }
            catch (Exception e)
            {

                return "";
            }

        }

        // Not yet implemented in CRIP
        public void WriteIdToNfcSmartcard(string id)
        {
            var user = new NfcUser { ID = id };

            string jsonObj = JsonConvert.SerializeObject(user);

            HttpWebRequest request = CreateWebRequest(CripAddress + "nfc/user");
            request.Method = "POST";
            request.ContentLength = jsonObj.Length;
            request.ContentType = "text/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(jsonObj);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                }
            }
        }

        public void ScanForDevices()
        {
            //Console.WriteLine("Scanning for devices..");
            HttpWebRequest request = CreateWebRequest(CripAddress + "scan");

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var responseValue = string.Empty;

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                    throw new ApplicationException(message);
                }

                // grab the response  
                using (var responseStream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(responseStream))
                    {
                        responseValue = reader.ReadToEnd();
                    }
                }

                //Console.WriteLine(responseValue);
            }
        }

        public void StartWcfService()
        {
            _host = new WebServiceHost(typeof(Crip2CaalhpService), new Uri(CripAddress));
            _sep = _host.AddServiceEndpoint(typeof (ICrip2CaalhpService), new WebHttpBinding(),
                CripAddress);
            _host.Open();           
        }

        public void UserAuthenticated(UserAuth user)
        {
            //
        }

        public void StopWcfService()
        {
            if (_host != null)
            {
                _host.Close();
                _sep = null;
            }
        }
    }
}

