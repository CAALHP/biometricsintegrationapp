﻿using System.Runtime.Serialization;

namespace WcfService
{
    [DataContract]
    public class UserAuth
    {
        [DataMember]
        public string id { get; set; }
        [DataMember]
        public string authtype { get; set; }
    }
}
