﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace WcfService
{
    [ServiceContract]
    public interface ICrip2CaalhpService
    {
        [OperationContract, WebInvoke(

            RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json,

            UriTemplate = "/user/auth",
            Method = "POST")]
        void CripBiometricsScanned(UserAuth user);
    }
}
