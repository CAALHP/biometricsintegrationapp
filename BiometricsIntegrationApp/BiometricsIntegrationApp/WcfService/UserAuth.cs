﻿using System.Runtime.Serialization;

namespace BiometricsIntegrationApp.WcfService
{
    [DataContract]
    public class UserAuth
    {
        [DataMember]
        public string userid { get; set; }
        [DataMember]
        public string authtype { get; set; }
    }
}
