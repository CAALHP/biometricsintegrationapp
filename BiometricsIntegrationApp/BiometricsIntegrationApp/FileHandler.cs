﻿using System;
using System.Globalization;
using System.IO;
using System.Windows;

namespace BiometricsIntegrationApp
{
    public class FileHandler
    {
        private string _filePath;

        public FileHandler(string filePath)
        {
            _filePath = filePath;
        }

        public FileHandler(Outputter filePath)
        {
            _filePath = @"biometrics.bio";
        }

        public void WriteStringToFile(string str, int id)
        {
            using (var file = new System.IO.StreamWriter(_filePath))
            {
                string append = @"""userid"":" + @"""" + id + @""",";

                var appendedString = str.Insert(1, append);
                file.WriteLine(appendedString);      
            }                
        }

        public string GetJsonStringFromFile()
        {
            string result = null;
            try
            {
                using (StreamReader sr = new StreamReader(_filePath))
                {
                    String line = sr.ReadToEnd();
                    result = result + line;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            return result;
        }
    }     
}
