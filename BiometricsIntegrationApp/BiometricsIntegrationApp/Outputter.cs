﻿using System.Windows.Controls;

namespace BiometricsIntegrationApp
{
    public class Outputter
    {
        private readonly TextBox _tb;

        public Outputter(TextBox tb)
        {
            _tb = tb;
        }

        public void Output(string value)
        {
            if (value != null)
            {
                _tb.Text = value + "\n\n" + _tb.Text;
            }
        }
    }
}
