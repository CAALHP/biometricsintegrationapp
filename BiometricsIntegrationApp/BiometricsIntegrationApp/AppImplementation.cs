﻿using System;
using System.Collections.Generic;
using System.Windows;
using caalhp.Core.Contracts;

namespace BiometricsIntegrationApp
{
    class AppImplementation : IAppCAALHPContract
    {
        private const string AppName = "BiometricsIntegrationApp";
        private readonly MainWindow _mainWindow;
        private IAppHostCAALHPContract _host;
        private int _processId;

        public AppImplementation(MainWindow mainWindow)
        {
            _mainWindow = mainWindow;
        }

        public AppImplementation()
        {
            _mainWindow = new MainWindow();
        }
        public void Notify(KeyValuePair<string, string> notification)
        {
            // No events at the momemt
        }

        public string GetName()
        {
            return AppName;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            Environment.Exit(0);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
        }

        private void DispatchToApp(Action action)
        {
            Application.Current.Dispatcher.Invoke(action);
        }

        public void Show()
        {
            DispatchToApp(() =>
            {
                //DoTheMessagePump();
                var window = Application.Current.MainWindow;
                if (!window.IsVisible)
                {
                    window.Show();
                }
                window.WindowState = WindowState.Maximized;
                window.Activate();
                window.Topmost = true;
                window.Topmost = false;
                window.Focus();
                //DoTheMessagePump();
            });
        }
    }
}
