﻿using System.IO;
using System.Net;
using System.Text;

namespace BiometricsIntegrationApp
{
    public class MarketplaceBroker
    {
        private Outputter _op;

        public MarketplaceBroker(Outputter op)
        {
            _op = op;
        }

        private const string Url = "http://10.42.0.60:60774/";

        public void SendUserToMarketplace(string str, int id)
        {
            string append = @"""userid"":" + @"""" + id + @""",";

            var appendedString = str.Insert(1, append);
            UserAddPost(appendedString);
            _op.Output("User sent to marketplace.");
        }

        private void UserAddPost(string str)
        {
            //HttpWebRequest request = new HttpWebRequest();.Create(Url + "user/templates");
            var request = (HttpWebRequest)WebRequest.Create(Url + "user/add");
            request.Method = "POST";
            request.ContentLength = str.Length;
            request.ContentType = "application/json";

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(str);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                    if (httpResponse.StatusCode.ToString() == "200")
                    {
                        _op.Output("User added to MarketPlace.");
                    }
                }
            }
        }

        public string GetAllUsers()
        {
            _op.Output("Fetching users from Marketplace.");
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + "user/templates");

            // Set credentials to use for this request.
            request.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // Get the stream associated with the response.
            Stream receiveStream = response.GetResponseStream();

            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            string responseString = readStream.ReadToEnd();

            response.Close();
            readStream.Close();

            _op.Output("All users fetched.");

            return responseString;
        }
    }
}
