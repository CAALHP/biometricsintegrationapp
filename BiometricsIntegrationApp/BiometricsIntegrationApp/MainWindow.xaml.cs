﻿using System.Threading.Tasks;
using System.Windows;
using BiometricsIntegrationApp.WcfService;
using caalhp.IcePluginAdapters;
using Exception = System.Exception;


namespace BiometricsIntegrationApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CripBroker _cb;
        private FileHandler _fh;
        private MarketplaceBroker _mb;
        private Outputter _op;


        public MainWindow()
        {
            InitializeComponent();

            // Subcribe to WCFService event
                        
            // CAALHP specific boostrapping
            const string endpoint = "localhost";
            try
            {
                var appImp = new AppImplementation(this);
                var adapter = new AppAdapter(endpoint, appImp);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }

            _op = new Outputter(TbStatus);
            _cb = new CripBroker(this, _op);
            _fh = new FileHandler(_op);
            _mb = new MarketplaceBroker(_op);
            
            _cb.StartWcfService();
            Initialize();
        }

        private async void BtnAddUser_Click(object sender, RoutedEventArgs e)
        {
            int userId = 0;
            if (TbUserID.Text != "Enter UserID" && TbUserID.Text != "")
            {
                try
                {
                    userId = int.Parse(TbUserID.Text);
                }
                catch (Exception)
                {
                    _op.Output("UserId has to be an integer.");
                    return;
                }                
            }
            else
            {
                _op.Output("Please enter an ID in the textbox.");
                return;
            }

            _op.Output("Please place finger on biometric scanner.");

            string responseValue = await GetTemplates();
            //string responseValue = _fh.GetJsonStringFromFile();

            if (responseValue == "")
            {
                _op.Output("No connection to CRIP");
            }
            else
            {
                _op.Output("Templates recieved: \n" + responseValue);
                _op.Output("Saving templates to file.");

                //_fh.WriteStringToFile(responseValue, userId);
                _mb.SendUserToMarketplace(responseValue, userId);

                string users = _mb.GetAllUsers();
                _cb.StoreBiometricsInCrip(users);
            }
        }

        private async void Initialize()
        {
            _op.Output("Testing connection to CRIP. Please wait...");
            await CheckConnection();
        }
        
        private async Task CheckConnection()
        {
            var result = await Task.Run(() => _cb.CheckConnection());
            if (result)
            {
                _op.Output("CRIP connection established");
            }
            else
            {
                _op.Output("CRIP connection failed.");
            }
        }

        private async Task<string> GetTemplates()
        {
            var result = await Task.Run(() => _cb.GetTemplates());
            return result;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _cb.StopWcfService();
        }

        public void UserAuthed(UserAuth user)
        {
            _op.Output("User with id: " + user.userid + " authenticated.\n");
        }
    }
}
